<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use JoomlaHasher\JoomlaHasher;
use Illuminate\Support\Str;
use App\Models\Account\User;

class JoomlaPasswordEncryptionTest extends TestCase
{
	use DatabaseTransactions;
	
	public function testPasswordEncyptionWithJoomlaHasher()
	{
		$joomlaHasher = new JoomlaHasher();
		
		$password = Str::random(10);
		
		$encryptedPassword = JoomlaHasher::generateHashedPassword($password);
		
		$this->assertCount(2, explode(':', $encryptedPassword), "Expecting joomla's password sult seperation with ':'");
		
		$this->assertTrue($joomlaHasher->check($password, $encryptedPassword));
	}
	
	public function testCreateUserWithJoomlaHasher()
    {
    	$password = Str::random(10);
    	$email = Str::random(10).'@mail.com';
    	
    	$user = factory(\App\User::class)->create(["email"=>$email, "password" => bcrypt($password),'api_token'=>uniqid(), 'status' => 'Active']);
    	
    	$this->assertCount(2, explode(':', $user->password), "Expecting joomla's password sult seperation with ':'");
    	
    	$this->assertTrue(Auth::attempt(['email' => $email, 'password' => $password, 'status' => 'Active']));
    }
}

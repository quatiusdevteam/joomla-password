<?php

namespace Quatius\JoomlaHasher\Models;

use RuntimeException;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;

class JoomlaHasher implements HasherContract
{
    /**
     * Hash the given value.
     *
     * @param  string  $value
     * @param  array   $options
     * @return string
     *
     * @throws \RuntimeException
     */
    public function make($value, array $options = [])
    {
        //return self::generateHashedPassword($value);
		return password_hash($value, PASSWORD_DEFAULT);
    }

    /**
     * Generate password with or without salt
     * 
     * @param string $password
     * @param string $salt
     * @return string
     */
    public static function generateHashedPassword($password, $salt="")
    {
    	if ($salt == "")
    		$salt = str_random(32);
    	
    	$hashedPassword = md5($password . $salt);
    	return $hashedPassword . ":" . $salt;
    }
    
    /**
     * Check the given plain value against a hash.
     *
     * @param  string  $value
     * @param  string  $hashedValue
     * @param  array   $options
     * @return bool
     */
    public function check($value, $hashedValue, array $options = [])
    {
        if (strlen($hashedValue) === 0) {
            return false;
        }
        
		if ($hashedValue[0] == '$')
		{
			return password_verify($value, $hashedValue);
		}
		else
		{
			$hashparts = explode(':' , $hashedValue); // You split up the password hash and the salt:
			if (count($hashparts)!=2)
				return false;
			
			$userhash = self::generateHashedPassword($value, $hashparts[1]);
			return $userhash == $hashedValue;
		}
		return false;
    }

    /**
     * Check if the given hash has been hashed using the given options.
     *
     * @param  string  $hashedValue
     * @param  array   $options
     * @return bool
     */
    public function needsRehash($hashedValue, array $options = [])
    {
        return self::generateHashedPassword($hashedValue);
    }
}
